<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            'name'=> 'Daniela Davila',
            'email'=> 'daniela@gmail.com',
            'password'=> bcrypt('12345'),
        ]);   

        DB::table('heuristicas')->insert([
            ['name' => 'Visibilidad y estado del sistema'],
            ['name' => 'Relacion entre el sistema y el mundo real'],
            ['name' => 'Control y libertad del usuario'],
            ['name' => 'Consistencia y estandares'],
            ['name' => 'Prevencion de errores'],
            ['name' => 'Reconocimiento antes que memoria'],
            ['name' => 'Flexibilidad y eficiencia de uso'],
            ['name' => 'Estetica y diseño minimalista'],
            ['name' => 'Reconocimiento y recuperacion de errores'],
            ['name' => 'Ayuda y documentacion'],

        ]);

        DB::table('variables')->insert([
            //H1. Visibilidad y Estado del Sistema
            ['descripcion'=>'El sistema mantiene al usuario informado sobre el estatus de alguna acción que ha realizado', 'heuristica_id'=>1],
            ['descripcion'=>'El progreso es visible mediante barras de avance o gifs', 'heuristica_id ' => 1],
            //H2. Relación entre el sistema y el mundo real
            ['descripcion'=>'El sistema usa términos, lenguaje y conceptos que son familiares para el usuario', 'heuristica_id ' => 2],
            ['descripcion'=>'El sistema utiliza convenciones del mundo real o metáforas para transmitir el uso correcto', 'heuristica_id' => 2],
            ['descripcion'=>'El sistema utiliza un orden natural y lógico', 'heuristica_id' => 2],
            //H3. Control y libertad del usuario
            ['descripcion'=>'El sistema permite hace sentir al usuario que puede controlar el flujo o la operación', 'heuristica_id' => 3],
            ['descripcion'=>'El sistema permite al usuario deshacer alguna acción a través de una “salida de emergencia”', 'heuristica_id' => 3],
            //H4. Consistencia y estándares
            ['descripcion'=>'Los botones son consistentes (tamaño, posición, estilo) a lo largo de la operación', 'heuristica_id' => 4],
            ['descripcion'=>'Los fonts son consistentes (tamaño, posición, estilo) a lo largo de la operación', 'heuristica_id' => 4],
            ['descripcion'=>'Los gráficos son consistentes (tamaño, posición, estilo) a lo largo de la operación', 'heuristica_id' => 4],
            //H5. Prevención de errores
            ['descripcion'=>'El sistema previene que el usuario cometa algún error o llegue a una situación donde se sienta comprometido a realizar alguna acción', 'heuristica_id' => 5],
            ['descripcion'=>'El sistema muestra mensajes claros o mecanismos para prevenir errores', 'heuristica_id' => 5],
            //H6. Reconocimiento antes que memoria
            ['descripcion'=>'El sistema evita que el usuario tenga que recurrir a la memoria durante la operación', 'heuristica_id' => 6],
            ['descripcion'=>'El sistema ayuda al usuario a reconocer lo que puede hacer', 'heuristica_id' => 6],
            ['descripcion'=>'El sistema da opciones visibles y adecuadas para ayudar al usuario a tomar decisiones', 'heuristica_id' => 6],
            ['descripcion'=>'El sistema da opciones para comparar alternativas sin tener que memorizar información', 'heuristica_id' => 6],
            //H7. Flexibilidad y eficiencia de uso
            ['descripcion'=>'El sistema permite deshacer, rehacer y revertir acciones', 'heuristica_id' => 7],
            ['descripcion'=>'El sistema da opciones especializadas para usuarios de nivel avanzado', 'heuristica_id' => 7],
            //H8. Estética y diseño minimalista
            ['descripcion'=>'Los gráficos, fotos y elementos no son solamente decorativos y cumplen una función o propósito', 'heuristica_id' => 8],
            ['descripcion'=>'Existen espacios blancos o negativos para descansar la vista', 'heuristica_id' => 8],
            ['descripcion'=>'Existe armonía visual y de sentido estético', 'heuristica_id' => 8],
            //H9. Reconocimiento y recuperación de errores
            ['descripcion'=>'El sistema permite al usuario recuperarse de un error', 'heuristica_id' => 9],
            ['descripcion'=>'El sistema muestra claramente dónde se ha cometido el error', 'heuristica_id' => 9],
            ['descripcion'=>'El sistema muestra claramente qué ha ocasionado el error', 'heuristica_id' => 9],
            ['descripcion'=>'El sistema es claro en los mensajes de error y es proactivo al sugerir cómo recuperarse', 'heuristica_id' => 9],
            //H10. Ayuda y documentación
            ['descripcion'=>'El sistema facilita varias formas de contacto (teléfono, email, "call me back", oficinas)', 'heuristica_id' => 10],
            ['descripcion'=>'El sistema ofrece la opción de atención en línea o videollamada (skype, chat, gtalk)', 'heuristica_id' => 10],
            ['descripcion'=>'El sistema ofrece una zona específica para resolver dudas y ésta es completa (FAQ, Glosario, Asistente virtual)', 'heuristica_id' => 10],
            ['descripcion'=>'Las preguntas frecuentes se encuentran bien redactadas', 'heuristica_id' => 10],
            ['descripcion'=>'El sistema ofrece ayudas contextualizadas en las páginas claves (ficha de producto y formularios)', 'heuristica_id' => 10],

        ]);

    }
}

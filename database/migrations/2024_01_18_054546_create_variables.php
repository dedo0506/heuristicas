<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('heuristica_id');
            $table->foreign('heuristica_id')->references('id')->on('heuristicas')->onDelete('cascade');
            $table->string('descripcion')->nullable();
            $table->float('puntuacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('variable');
    }
};

<div>
    <?
    ?>

    <h1 class="text-xl font-mono text-center">EVALUACION HEURISTICA</h1>
    <br>
    <table class="table-auto border-collapse border border-slate-500";>
        {{-- Mostrar las heuristicas --}}
        @foreach ($heuristicas as $heuristica)
            <thead>
                <th class="border-collapse border border-slate-500" colspan="7" bgcolor="cornflowerblue">
                    H.{{ $heuristica->id }} {{ $heuristica->name }}</th>
                <tr>
                    {{-- <th scope="col">Heu</th> --}}
                    {{-- <th class="border-collapse border border-slate-500" scope="col"> </th> --}}
                    <th class="border-collapse border border-slate-500" scope="col" colspan="2">Variable</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="pink">No Representa esta
                        caracteristica [0]</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="moccasin">Cumple con lo
                        minimo y con deficiencias [1]</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="paleturquoise">Cumple
                        ofreciendo caracteristicas estandar [2]</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="palegreen">Se distingue
                        con buena práctica [3]</th>
                </tr>
            </thead>
            <tbody>
                {{-- Mostrar las variables que corresponden a esas heuristicas --}}
                @foreach ($heuristica->variables as $variable)

                    <tr>
                        <td class="border-collapse border border-slate-500">{{ $variable->id }}</td>
                        <td class="border-collapse border border-slate-500">{{ $variable->descripcion }}</td>
                    @if (count($variable->evaluaciones) > 0)
                        @foreach ($variable->evaluaciones as $evaluacion)
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="0"
                                    wire:click="promedio({{ $variable->id }}, 0)"
                                    {{ old($variable->puntuacion, $evaluacion->puntuacion) }}>
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="1"
                                    wire:click="promedio({{ $variable->id }}, 1)"
                                    {{ old($variable->puntuacion, $evaluacion->puntuacion) }}>
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="2"
                                    wire:click="promedio({{ $variable->id }}, 2)"
                                    {{ old($variable->puntuacion, $evaluacion->puntuacion) }}>
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="3"
                                    wire:click="promedio({{ $variable->id }}, 3)"
                                    {{ old($variable->puntuacion, $evaluacion->puntuacion) }}>
                            </td>
                        </tr>            
                        @endforeach
                    
                    @else
                        <tr>
                            <td class="border-collapse border border-slate-500">{{ $variable->id }}</td>
                            <td class="border-collapse border border-slate-500">{{ $variable->descripcion }}</td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="0"
                                    wire:click="promedio({{ $variable->id }}, 0)">
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="1"
                                    wire:click="promedio({{ $variable->id }}, 1)">
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="2"
                                    wire:click="promedio({{ $variable->id }}, 2)">
                            </td>
                            <td class="border-collapse border border-slate-500 text-center">
                                <input type="radio" name="{{ $variable->id }}" value="3"
                                    wire:click="promedio({{ $variable->id }}, 3)">
                            </td>
                        </tr>
                        @endif
            </tbody-->
                <tr>
                    <td colspan="5" class="text-right">Promedio</td>
                    <td class="text-center">
                        {{ number_format($variable->where('heuristica_id', $heuristica->id)->avg('puntuacion'), 2) }}
                    </td>
                </tr>
        @endforeach
    </table>
    <div class="flex justify-end mr-4 my-4">
        <a href="{{ route('resultados-evaluacion-heuristica') }}" wire:click="limpiar">
            <x-button style="background: blue">
                Mostrar Resultados
            </x-button>
        </a>
    </div>

</div>

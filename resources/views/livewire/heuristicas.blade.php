<div>
    <h1 class="text-xl font-mono text-center">EVALUACION HEURISTICA</h1>
    <br>
    <table  class="table-auto border-collapse border border-slate-500"; >
        {{-- Mostrar las heuristicas --}}
        @foreach ($heuristicas as $heuristica)
            
            <thead> 
                <th class="border-collapse border border-slate-500" colspan="7" bgcolor="cornflowerblue">H.{{$heuristica->id}}  {{$heuristica->name}}</th>   
                <tr>
                    {{-- <th scope="col">Heu</th> --}}
                    {{-- <th class="border-collapse border border-slate-500" scope="col"> </th> --}}
                    <th class="border-collapse border border-slate-500" scope="col" colspan="2">Variable</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="pink">No Representa esta caracteristica [0]</th>
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="moccasin">Cumple con lo minimo y con deficiencias [1]</th> 
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="paleturquoise">Cumple ofreciendo caracteristicas estandar [2]</th> 
                    <th class="border-collapse border border-slate-500" scope="col" bgcolor="palegreen">Se distingue con buena práctica [3]</th> 
                </tr>
            </thead>
            <tbody>
        {{-- Mostrar las variables que corresponden a esas heuristicas --}}
            @foreach($heuristica->variables as $variable)
                <tr>
                    <td class="border-collapse border border-slate-500">{{$variable->id}}</td>
                    <td class="border-collapse border border-slate-500">{{$variable->descripcion}}</td>                     
                    <td class="border-collapse border border-slate-500 text-center">
                        <input type="radio" name="{{ $variable->id }}" value="0" wire:click="evaluacion({{ $variable->id }}, 0)" 
                        {{ old($variable->id, $variable->puntuacion) == '0' ? 'checked' : '' }}>
                    </td>
                    <td class="border-collapse border border-slate-500 text-center">
                        <input type="radio" name="{{ $variable->id }}" value="1" wire:click="evaluacion({{ $variable->id }}, 1)" 
                        {{ old($variable->id, $variable->puntuacion) == '1' ? 'checked' : '' }}>
                    </td>
                    <td class="border-collapse border border-slate-500 text-center">
                        <input type="radio" name="{{ $variable->id }}" value="2" wire:click="evaluacion({{ $variable->id }}, 2)" 
                        {{ old($variable->id, $variable->puntuacion) == '2' ? 'checked' : '' }}>
                    </td>
                    <td class="border-collapse border border-slate-500 text-center">
                        <input type="radio" name="{{ $variable->id }}" value="3" wire:click="evaluacion({{ $variable->id }}, 3)" 
                        {{ old($variable->id, $variable->puntuacion) == '3' ? 'checked' : '' }}>
                    </td>
                </tr>
            
            @endforeach
            </tbody-->
            <tr>
                <td colspan="5" class="text-right">Promedio</td>
                <td class="text-center">{{number_format($heuristica->variables->avg('puntuacion'),2)}}</td>
               
            </tr>
        @endforeach
        </table>
    {{--Para ver los resultados de cada heuristica hay que deshabilitarlo
    @livewire('ResultadosHeuristicas')--}}

    <div class="flex justify-end mr-4 my-4">
        <x-button style="background: blue" >
            Mostrar Resultados
        </x-button>
    </div>
    

</div>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {{-- <x-welcome /> --}}
                <section>

                    <h1 class="text-xl font-mono text-center">RESULTADOS DE LA EVALUACION HEURISTICA</h1>
                    <br>
                    <table class=" mx-auto my-4 table-auto border-collapse border border-slate-500 ";>
                        {{-- Mostrar las heuristicas --}}

                        <thead>
                            <tr>
                                <th class="border-collapse border border-slate-500" scope="col" colspan="2"
                                    bgcolor="cornflowerblue">Heuristica</th>
                                <th class="border-collapse border border-slate-500" bgcolor="cornflowerblue">Puntuacion
                                </th>
                            </tr>
                        </thead>
                        @foreach ($heuristicas as $heuristica)
                            <tbody>
                                <tr>
                                    <th class="border-collapse border border-slate-500">{{ $heuristica->id }}</th>
                                    <th class="border-collapse border border-slate-500">{{ $heuristica->name }}</th>
                                    <th class="border-collapse border border-slate-500">{{ $heuristica->promedio }}</th>
                                </tr>
                            </tbody>
                        @endforeach

                    </table>
                </section>
               {{--  <div>
                    <canvas id="myChart"></canvas>
                </div> --}}

            </div>
        </div>
    </div>
</x-app-layout>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
    const ctx = document.getElementById('myChart');

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>

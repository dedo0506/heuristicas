<?php

namespace App\Livewire;

use App\Models\Evaluacion;
use App\Models\Heuristica;
use App\Models\Variable;
use Livewire\Component;

class ResultadosHeuristicas extends Component
{
    public function render()
    {
        $evaluaciones =Evaluacion::all();
        $heuristicas = Heuristica::all();

        $variables = Variable::all();
        
        foreach ($heuristicas as $heuristica) {
            
            // Utiliza la función map para crear una colección de promedios de las variables
            $promediosVariables = $heuristica->variables->map(function ($variable) use ($evaluaciones) {
                return $evaluaciones->where('variable_id', $variable->id)->avg('puntuacion');
            });
        
            // Calcula el promedio general para la heurística
            $promedioHeuristica = $promediosVariables->avg();
        
            // Asigna el promedio general a la propiedad de la heurística
            $heuristica->promedio = number_format($promedioHeuristica, 2);
        }
        
        return view('livewire.resultadosHeuristicas', compact('heuristicas'));
    }
}

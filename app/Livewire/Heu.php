<?php

namespace App\Livewire;

use App\Models\Heuristica;
use App\Models\Variable;
use App\Models\Evaluacion;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Heu extends Component
{
    public $heuristicas;
    public $variables;
    public $evaluaciones;

    public function mount()
    {
        $variables = Variable::all(); 
        foreach($variables as $variable){                
            $variable->puntuacion = null;
        }
        $this->heuristicas = Heuristica::all();
        $this->evaluaciones = Evaluacion::where('user_id', Auth::id())->get();
        $this->variables = $variables; 
        
    }


    public function render()
    {
        return view('livewire.heu', [
            'heuristicas' => $this->heuristicas,
            'variables' => $this->variables,
            'evaluaciones'=>$this->evaluaciones
        ]);
    }

    public function promedio($variableId, $value){

        $evaluacion = Evaluacion::updateOrCreate(
            ['user_id' => Auth::id(), 'variable_id' => $variableId],
            ['puntuacion' => $value]
        );

        $variable = Variable::find($variableId);
        if ($variable) {
            $variable->update(['puntuacion' => $value]);
        }

        /* $this->promedio = number_format($variable->where($variable->heuristica_id)->avg('puntuacion'), 2); */
        
    }

}

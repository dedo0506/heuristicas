<?php

namespace App\Livewire;

use App\Models\Evaluacion;
use App\Models\Heuristica;
use App\Models\User;
use App\Models\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Heuristicas extends Component
{
    
    public function render()
    {
        
        $heuristicas = Heuristica::all();
        $variables = Variable::all(); 
        
        return view('livewire.heuristicas', compact('heuristicas', 'variables'));
    }

    public function evaluacion($variableId, $value){

        $evaluacion = Evaluacion::updateOrCreate(
            ['user_id' => Auth::id(), 'variable_id' => $variableId],
            ['puntuacion' => $value]
        );

        $variable = Variable::find($variableId);
        if ($variable) {
            $variable->update(['puntuacion' => $value]);
        }

    }

    public function resultados(){
        
        $evaluaciones =Evaluacion::all();
        $heuristicas = Heuristica::all();
        $variables = Variable::all();
        
        foreach ($heuristicas as $heuristica) {
            
            // Utiliza la función map para crear una colección de promedios de las variables
            $promediosVariables = $heuristica->variables->map(function ($variable) use ($evaluaciones) {
                return $evaluaciones->where('variable_id', $variable->id)->avg('puntuacion');
            });
        
            // Calcula el promedio general para la heurística
            $promedioHeuristica = $promediosVariables->avg();
        
            // Asigna el promedio general a la propiedad de la heurística
            $heuristica->promedio = number_format($promedioHeuristica, 2);
        }
        
        return view('livewire.resultadosHeuristicas', compact('heuristicas'));
    }

}

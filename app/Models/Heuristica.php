<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Heuristica extends Model
{
    use HasFactory;
    //propiedades del objeto que el usuario solo tiene acceso a modificar o guardar en la base
    protected $fillable = [];

        /*
    Propiedad que permite proteger el acceso a ciertos campos, es decir 
    ignora la informacion que se quiera agregar a ese campo si el usuario no tiene privilegios*/
    protected $guarded = [];

    //relacion 1:N con variables
    public function variables(){
        return$this->hasMany(Variable::class, 'heuristica_id','id');
    }
}

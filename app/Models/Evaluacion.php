<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    use HasFactory;
    //propiedades del objeto que el usuario solo tiene acceso a modificar o guardar en la base
    protected $fillable = [];

     /*
    Propiedad que permite proteger el acceso a ciertos campos, es decir 
    ignora la informacion que se quiera agregar a ese campo si el usuario no tiene privilegios*/
    protected $guarded = [];

    //relacion N:1 con variables
    public function variable()
    {
        return $this->belongsTo(Variable::class, 'variable_id', 'id');
    }

    //relacion 1:N con usuario
    public function usuario(){
        return$this->hasMany(User::class, 'evaluacion_id','id');
    }

}
